# OpenML dataset: Heart_disease_classification

https://www.openml.org/d/43944

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

This database contains 76 attributes, but all published experiments refer to using a subset of 14 of them.     The goal field refers to the presence of heart disease in the patient.    It is integer valued with 0 or 1.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43944) of an [OpenML dataset](https://www.openml.org/d/43944). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43944/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43944/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43944/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

